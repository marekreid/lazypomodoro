package main

import (
	tea "github.com/charmbracelet/bubbletea"
	"time"
)

type Question struct {
	question string
	answer   string
	input    Input
}

func NewQuestion(q string) Question {
	return Question{question: q}
}

func NewShortQuestion(q string) Question {
	question := NewQuestion(q)
	model := NewShortAnswerField()
	question.input = model
	return question
}

func NewLongQuestion(q string) Question {
	question := NewQuestion(q)
	model := NewLongAnswerField()
	question.input = model
	return question
}

type Main struct {
	styles    *Styles
	index     int
	questions []Question
	width     int
	height    int
	done      bool
	timer     *time.Ticker // Add timer to Main struct
}

func New(questions []Question) Main {
	styles := DefaultStyles()
	return Main{styles: styles, questions: questions}
}

func (m Main) Init() tea.Cmd {
	return m.questions[m.index].input.Blink
}

func (m Main) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	current := &m.questions[m.index]
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.width = msg.Width
		m.height = msg.Height
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		case "enter":
			// Save the current answer
			current.answer = current.input.Value()
			// Move to the next question or mark as done
			if m.index == len(m.questions)-1 {
				m.done = true
			} else {
				m.Next()
				// Focus on the next input
				return m, m.questions[m.index].input.Blink
			}
		}
	}
	current.input, cmd = current.input.Update(msg)
	return m, cmd
}

func (m *Main) Next() {
	if m.index < len(m.questions)-1 {
		m.index++
	} else {
		m.index = 0
	}
}

// Add the View method
func (m Main) View() string {
	return renderView(m)
}

