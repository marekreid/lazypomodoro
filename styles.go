package main

import (
	"github.com/charmbracelet/lipgloss"
)

type Styles struct {
	BorderColor      lipgloss.Color
	InputField       lipgloss.Style
	LeftColumnStyle  lipgloss.Style
	RightColumnStyle lipgloss.Style
	ViewStyle        lipgloss.Style // Add this line
}

func CatppuccinColors() map[string]string {
	return map[string]string{
		"rosewater": "#F5E0DC",
		"flamingo":  "#F2CDCD",
		"pink":      "#F5C2E7",
		"mauve":     "#CBA6F7",
		"red":       "#F38BA8",
		"maroon":    "#EBA0AC",
		"peach":     "#FAB387",
		"yellow":    "#F9E2AF",
		"green":     "#A6E3A1",
		"teal":      "#94E2D5",
		"sky":       "#89DCEB",
		"sapphire":  "#74C7EC",
		"blue":      "#89B4FA",
		"lavender":  "#B4BEFE",
	}
}

func DefaultStyles() *Styles {
	colors := CatppuccinColors()
	s := new(Styles)
	s.BorderColor = lipgloss.Color(colors["blue"])
	s.InputField = lipgloss.NewStyle().
		BorderForeground(s.BorderColor).
		BorderStyle(lipgloss.RoundedBorder()).
		Padding(1).
		Width(80)

	s.LeftColumnStyle = lipgloss.NewStyle().
		BorderForeground(s.BorderColor).
		BorderStyle(lipgloss.RoundedBorder()).
		Padding(1).
		Width(40)

	s.RightColumnStyle = lipgloss.NewStyle().
		BorderForeground(s.BorderColor).
		BorderStyle(lipgloss.RoundedBorder()).
		Width(40)

	s.ViewStyle = lipgloss.NewStyle().Padding(1) // Add padding to the view

	return s
}
