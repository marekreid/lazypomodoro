package main

import (
	"fmt"
	"github.com/charmbracelet/lipgloss"
)

func RenderLeftColumn(m Main, columnWidth int) string {
	colors := CatppuccinColors()

	// Heading style
	headingStyle := lipgloss.NewStyle().
		Bold(true).
		Underline(true).
		Foreground(lipgloss.Color(colors["mauve"])).
		Render("Interval values")

	// Styles for the questions and answers
	questionStyle := lipgloss.NewStyle().
		Bold(true).
		Foreground(lipgloss.Color(colors["peach"]))

	answerStyle := lipgloss.NewStyle().
		Foreground(lipgloss.Color(colors["green"]))

	// Add the heading
	output := headingStyle + "\n\n"

	// Format each question and answer
	for _, q := range m.questions {
		output += fmt.Sprintf("%s: %s\n", questionStyle.Render(q.question), answerStyle.Render(q.answer))
	}

	return m.styles.LeftColumnStyle.Copy().Width(columnWidth).Render(output)
}
