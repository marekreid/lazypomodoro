package main

import (
	"github.com/charmbracelet/lipgloss"
)

func renderView(m Main) string {
	if m.done {
		leftColumnWidth := 30                            // Width of the left column
		remainingWidth := m.width - leftColumnWidth - 10 // Adjust width for the gap

		leftColumn := RenderLeftColumn(m, leftColumnWidth)
		rightColumn := RenderRightColumn(m, remainingWidth)

		gap := lipgloss.NewStyle().Width(1).Render(" ")
		view := lipgloss.JoinHorizontal(lipgloss.Top, leftColumn, gap, rightColumn)

		return m.styles.ViewStyle.Render(view) // Apply the view style here
	}

	if m.width == 0 {
		return "loading..."
	}

	// Render current question and input field
	return m.styles.ViewStyle.Render( // Apply the view style here
		lipgloss.Place(
			m.width,
			m.height,
			lipgloss.Center,
			lipgloss.Center,
			lipgloss.JoinVertical(
				lipgloss.Left,
				m.questions[m.index].question,
				m.styles.InputField.Render(m.questions[m.index].input.View()),
			),
		),
	)
}
