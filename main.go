package main

import (
	"fmt"
	"log"
	"os"

	tea "github.com/charmbracelet/bubbletea"
)

func main() {
  CheckDeps()
	questions := []Question{
		NewShortQuestion("Interval time (mins)"),
		NewShortQuestion("Break Time (mins)"),
		NewShortQuestion("Number of intervals"),
	}
	mainModel := New(questions)

	f, err := tea.LogToFile("debug.log", "debug")
	if err != nil {
		fmt.Println("fatal:", err)
		os.Exit(1)
	}
	defer f.Close()

	p := tea.NewProgram(mainModel, tea.WithAltScreen())
	if _, err := p.Run(); err != nil {
		log.Fatal(err)
	}
}
