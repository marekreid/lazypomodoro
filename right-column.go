package main

import (
	"fmt"
	"os/exec"
	"strconv"
	"time"

	"github.com/charmbracelet/lipgloss"
)

var timerStarted = false // Add a global variable to ensure the timer starts only once
var currentInterval int  // Track the current interval globally in seconds
var currentBreak int     // Track the current break globally in seconds
var currentState string  // Track the current state: "work" or "break"
var remainingIntervals int
var intervalLength int // Duration of work interval in seconds
var breakLength int    // Duration of break interval in seconds

func RenderRightColumn(m Main, remainingWidth int) string {
	if !timerStarted && m.done {
		var err error
		intervalLength, err = strconv.Atoi(m.questions[0].answer)
		breakLength, err = strconv.Atoi(m.questions[1].answer)
		remainingIntervals, err = strconv.Atoi(m.questions[2].answer)
		if err != nil {
			fmt.Println("Error converting interval:", err)
		} else {
			currentInterval = intervalLength * 60 // Convert minutes to seconds
			currentBreak = breakLength * 60       // Convert minutes to seconds
			currentState = "work"
			startTimer()
			timerStarted = true
		}
	}

	// Play sounds and transition states
	if currentState == "work" && currentInterval == 0 {
		if remainingIntervals > 0 {
			playSound("abort.wav")
			currentState = "break"
			currentBreak = breakLength * 60 // Reset break interval
		}
	} else if currentState == "break" && currentBreak == 0 {
		remainingIntervals -= 1
		if remainingIntervals > 0 {
			playSound("out_of_gum_x.wav")
			currentState = "work"
			currentInterval = intervalLength * 60 // Reset work interval
		}
	}

	// Generate large text for the remaining time using figlet with doom font
	remainingTime := currentInterval
	if currentState == "break" {
		remainingTime = currentBreak
	}
	numberOut, err := generateLargeText(fmt.Sprintf("%02d", remainingTime))
	// largeTecxt, err := generateLargeText(fmt.Sprintf("Remaining interval time"))
	largeText := "Remaining interval\n\n" + numberOut
	if err != nil {
		fmt.Println("Error generating text:", err)
		largeText = "Error generating text"
	}

	// Catppuccin color scheme
	colors := CatppuccinColors()

	// Styles for the right column content
	contentStyle := lipgloss.NewStyle().
		Foreground(lipgloss.Color(colors["sky"]))

	// Style for the right column
	rightColumnStyle := m.styles.RightColumnStyle.Copy().Width(remainingWidth)

	// Render the large text with the defined style
	centeredText := lipgloss.Place(
		remainingWidth, // Width of the right column
		m.height-3,     // Height of the terminal
		lipgloss.Center,
		lipgloss.Center,
		largeText,
	)

	return rightColumnStyle.Render(contentStyle.Render(centeredText))
}

func startTimer() {
	ticker := time.NewTicker(1 * time.Second)
	go func() {
		for range ticker.C {
			if currentState == "work" && currentInterval > 0 {
				currentInterval -= 1
			} else if currentState == "break" && currentBreak > 0 {
				currentBreak -= 1
			}
		}
	}()
}

func generateLargeText(text string) (string, error) {
	// Use the local path to the doom font file
	fontPath := "./doom.flf"

	out, err := exec.Command("figlet", "-f", fontPath, text).Output()
	if err != nil {
		return "", fmt.Errorf("error executing figlet: %w", err)
	}
	return string(out), nil
}

func playSound(fileName string) {
	var cmd *exec.Cmd

	cmd = exec.Command("paplay", fileName)

	err := cmd.Run()
	if err != nil {
		fmt.Println("Error playing sound:", err)
	}
}

