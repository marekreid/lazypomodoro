package main

import (
    "fmt"
    "log"
    "os/exec"
    "runtime"
)



func CheckDeps() {
    linuxDependencies := []string{"figlet", "paplay"}
    osxDependencies := []string{"figlet", "afplay"}

    var dependenciesList []string

    os := checkOs()
    switch os {
    case "linux":
        dependenciesList = linuxDependencies
    case "osx":
        dependenciesList = osxDependencies
    }

    for _, d := range dependenciesList {
        if !depIsInstalled(d) {
            fmt.Printf("Dependency %s is not installed.\n", d)
            // Install dependency 
            if msg, err := installDependency(d, os); err != nil {
                fmt.Println("Error:", err)
            } else {
                fmt.Println(msg)
            }
        }
    }
}

func depIsInstalled(d string) bool {
    cmd := exec.Command("which", d)
    if err := cmd.Run(); err != nil {
        return false
    }
    return true
}

func checkOs() string {
    os := runtime.GOOS
    switch os {
    case "linux":
        return "linux"
    case "darwin":
        return "osx"
    case "windows":
        log.Fatal("Ya shit outta luck sorry pal. Ya running windows and I ain't got any sympathy for ya sorry =/")
    default:
        log.Fatal("EEEK ur running an unknown OS sorry bra, u shit outta luck")
    }
    return "" // This line is never reached because log.Fatal exits the program
}

func installDependency(dep string, os string) (string, error) {
    fmt.Printf("Dependency %s is now being installed...\n", dep)

    var cmd *exec.Cmd
    switch os {
    case "linux":
        cmd = exec.Command("sudo", "apt-get", "install", "-y", dep)
    case "osx":
        cmd = exec.Command("brew", "install", dep)
    default:
        return "", fmt.Errorf("Failed to install %s: unsupported os", dep)
    }

    if err := cmd.Run(); err != nil {
        return "", fmt.Errorf("Failed to install %s: %v", dep, err)
    }

    return fmt.Sprintf("Dependency %s installed successfully", dep), nil
}

